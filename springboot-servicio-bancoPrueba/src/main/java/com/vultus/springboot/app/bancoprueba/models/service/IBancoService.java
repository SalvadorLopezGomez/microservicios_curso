package com.vultus.springboot.app.bancoprueba.models.service;

import java.util.List;

import com.vultus.springboot.app.bancoprueba.models.entity.Banco;

public interface IBancoService {
	
	public List<Banco> findAll();
	public Banco findByClave(Integer clave);
}
