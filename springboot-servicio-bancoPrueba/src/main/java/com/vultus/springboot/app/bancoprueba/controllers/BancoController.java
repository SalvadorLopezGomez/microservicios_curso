package com.vultus.springboot.app.bancoprueba.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vultus.springboot.app.bancoprueba.models.entity.Banco;
import com.vultus.springboot.app.bancoprueba.models.service.IBancoService;

@RestController
public class BancoController {
	@Autowired
	private Environment env;
	
	@Autowired
	private IBancoService bancoService;
	
	@GetMapping("/listar")
	public List<Banco> listar(){
		return bancoService.findAll().stream().map(banco -> {
			banco.setPort(Integer.parseInt(env.getProperty("local.server.port")));
			return banco;
		}).collect(Collectors.toList());
	}
	
	@GetMapping("/banco/{clave}")
	public Banco nombre(@PathVariable Integer clave){
		Banco banco = bancoService.findByClave(clave);
		banco.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		/*try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return banco;
	}
}
