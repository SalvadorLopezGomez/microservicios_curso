package com.vultus.springboot.app.bancoprueba.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vultus.springboot.app.bancoprueba.models.dao.BancoDao;
import com.vultus.springboot.app.bancoprueba.models.entity.Banco;

@Service
public class BancoServiceImpl implements IBancoService{
	
	@Autowired
	private BancoDao bancoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Banco> findAll() {
		return (List<Banco>) bancoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Banco findByClave(Integer clave) {
		return bancoDao.findById(clave).orElse(null);
	}

}
