package com.vultus.springboot.app.bancoprueba.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.vultus.springboot.app.bancoprueba.models.entity.Banco;

public interface BancoDao extends CrudRepository<Banco, Integer>{
	
}
