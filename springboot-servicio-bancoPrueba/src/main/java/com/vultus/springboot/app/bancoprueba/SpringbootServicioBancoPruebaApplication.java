package com.vultus.springboot.app.bancoprueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringbootServicioBancoPruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioBancoPruebaApplication.class, args);
	}

}
