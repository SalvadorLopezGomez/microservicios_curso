package com.vultus.springboot.app.registro.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vultus.springboot.app.registro.models.entity.Usuario;
import com.vultus.springboot.app.registro.models.service.IRegistroService;

@RestController
public class RegistroController {
	
	@Autowired
	private IRegistroService registroService;

	@GetMapping("/usuarios")
	public List<Usuario> listar(){
		return registroService.findAll();
	}
	
	@GetMapping("/usuario/{id}")
	public Usuario detalle(@PathVariable Long id) {
		return registroService.findById(id);
	}
}
