package com.vultus.springboot.app.registro.models.service;

import java.util.List;

import com.vultus.springboot.app.registro.models.entity.Usuario;

public interface IRegistroService {
	
	public List<Usuario> findAll();
	public Usuario findById(Long id);
}
