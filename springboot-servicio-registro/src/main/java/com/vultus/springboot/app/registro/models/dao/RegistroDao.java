package com.vultus.springboot.app.registro.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.vultus.springboot.app.registro.models.entity.Usuario;

public interface RegistroDao extends CrudRepository<Usuario, Long>{

}
