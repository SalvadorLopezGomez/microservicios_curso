package com.vultus.springboot.app.cuentas.models;

public class Cuenta {

	private Banco banco;
	private String bancaria;
	private Integer dinero;
	
	public Cuenta() {
	}

	public Cuenta(Banco banco, String bancaria, Integer dinero) {
		this.banco = banco;
		this.bancaria = bancaria;
		this.dinero = dinero;
	}
	
	public Integer getDinero() {
		return dinero;
	}

	public void setDinero(Integer dinero) {
		this.dinero = dinero;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getBancaria() {
		return bancaria;
	}

	public void setBancaria(String bancaria) {
		this.bancaria = bancaria;
	}
	
	public Double getGasto() {
		return dinero.doubleValue() - 15;
	}
}
