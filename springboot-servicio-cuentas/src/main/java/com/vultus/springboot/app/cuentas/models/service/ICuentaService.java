package com.vultus.springboot.app.cuentas.models.service;

import java.util.List;

import com.vultus.springboot.app.cuentas.models.Banco;
import com.vultus.springboot.app.cuentas.models.Cuenta;

public interface ICuentaService {
	
	public List<Cuenta> findAll();
	public Cuenta findById(Integer id,String alias, Integer dinero);
	public List<Object> bancosR();
	public Object findByInst(Integer key);
}
