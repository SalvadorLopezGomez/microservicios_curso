package com.vultus.springboot.app.cuentas.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.vultus.springboot.app.cuentas.models.Banco;

@FeignClient(name="servicio-bancoprueba")
public interface BancoClienteRest {
	
	@GetMapping("/listar")
	public List<Banco> listar();
	
	@GetMapping("/banco/{clave}")
	public Banco nombre(@PathVariable Integer clave);
}
